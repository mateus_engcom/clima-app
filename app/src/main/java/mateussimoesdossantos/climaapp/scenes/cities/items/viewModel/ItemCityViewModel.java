package mateussimoesdossantos.climaapp.scenes.cities.items.viewModel;

import android.databinding.ObservableField;
import android.view.View;

import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.scenes.cities.activities.WeatherDetailsCityActivity;

/**
 * Created by mateus on 15/08/18.
 */

public class ItemCityViewModel {
    public ObservableField<City> city = new ObservableField<>();

    public ItemCityViewModel(City city) {
        this.city.set(city);
    }

    public void itemClicked(View v) {
        WeatherDetailsCityActivity.openActivity(v.getContext(), city.get().getId());
    }
}
