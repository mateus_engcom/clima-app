package mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel;

import mateussimoesdossantos.climaapp.scenes.base.BaseNavigator;

public interface WeatherDetailsCityActivityNavigator extends BaseNavigator {
    void setFavoredStatus(boolean isFavored);
}
