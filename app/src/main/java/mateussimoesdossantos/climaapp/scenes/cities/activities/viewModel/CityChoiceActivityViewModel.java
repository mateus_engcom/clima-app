package mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel;

import com.genius.groupie.GroupAdapter;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.model.CityModel.CityData;
import mateussimoesdossantos.climaapp.scenes.cities.items.ItemCity;

import static mateussimoesdossantos.climaapp.app.Constants.fileListJson;
import static mateussimoesdossantos.climaapp.utils.Utils.loadJSONFromAsset;


public class CityChoiceActivityViewModel {

    public final GroupAdapter citiesAdapter = new GroupAdapter();
    CityChoiceActivityNavigator navigator;

    public CityChoiceActivityViewModel(CityChoiceActivityNavigator navigator) {
        this.navigator = navigator;
        initVariables();
    }

    public void initVariables() {
        fillCitiesAdapter();
    }

    private void fillCitiesAdapter() {
        List<City> cities = getCities();
        Collections.sort(cities, (object1, object2) -> (object1.getName()).compareTo((object2.getName())));

        for (City city : cities) {
            citiesAdapter.add(new ItemCity(city));
        }
        citiesAdapter.notifyDataSetChanged();
    }

    public List<City> getCities() {
        List<City> cities = new ArrayList<>();
        //need implementation
        String jsonList = loadJSONFromAsset(navigator.getContext(), fileListJson);
        if (jsonList != null) {
            CityData cityData = new Gson().fromJson(jsonList, CityData.class);
            return cityData.getCities();
        }
        return cities;
    }
}
