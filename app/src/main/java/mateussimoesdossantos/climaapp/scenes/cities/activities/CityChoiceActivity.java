package mateussimoesdossantos.climaapp.scenes.cities.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;

import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.databinding.ActivityCityChoiceBinding;
import mateussimoesdossantos.climaapp.scenes.base.BaseActivity;
import mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel.CityChoiceActivityNavigator;
import mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel.CityChoiceActivityViewModel;


public class CityChoiceActivity extends BaseActivity implements CityChoiceActivityNavigator {

    ActivityCityChoiceBinding mBinding;
    CityChoiceActivityViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_city_choice);
        if (mViewModel == null)
            mViewModel = new CityChoiceActivityViewModel(CityChoiceActivity.this);

        mBinding.setViewModel(mViewModel);
        setToolbar(R.string.city_choice);
    }
}
