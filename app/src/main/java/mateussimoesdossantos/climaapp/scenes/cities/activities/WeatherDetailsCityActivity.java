package mateussimoesdossantos.climaapp.scenes.cities.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.databinding.ActivityWeatherDetailsCityBinding;
import mateussimoesdossantos.climaapp.scenes.base.BaseActivity;
import mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel.WeatherDetailsCityActivityNavigator;
import mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel.WeatherDetailsCityActivityViewModel;


public class WeatherDetailsCityActivity extends BaseActivity implements WeatherDetailsCityActivityNavigator {

    static public String CITY_EXTRA = "city_extra";
    ActivityWeatherDetailsCityBinding mBinding;
    WeatherDetailsCityActivityViewModel mViewModel;

    static public void openActivity(Context context, long cityId) {
        Intent intent = new Intent(context, WeatherDetailsCityActivity.class);
        intent.putExtra(CITY_EXTRA, cityId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_weather_details_city);
        if (mViewModel == null)
            mViewModel = new WeatherDetailsCityActivityViewModel(WeatherDetailsCityActivity.this, getIntent().getLongExtra(CITY_EXTRA, -1));

        mBinding.setViewModel(mViewModel);
        setToolbar(R.string.details);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.weather_details_city_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_to_favor || item.getItemId() == R.id.action_remove_favor) {
            //need implementation
            mViewModel.favorClicked();
        }
        mViewModel.updateFavoredState();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        setFavoredStatus(mViewModel.isFavored());
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setFavoredStatus(boolean isFavored) {
        if (toolbar != null && toolbar.getMenu() != null) {
            toolbar.getMenu().findItem(R.id.action_to_favor).setVisible(!isFavored);
            toolbar.getMenu().findItem(R.id.action_remove_favor).setVisible(isFavored);
        }
    }
}
