package mateussimoesdossantos.climaapp.scenes.cities.activities.viewModel;

import android.databinding.ObservableField;

import io.realm.Realm;
import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.service.RetrofitBase;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WeatherDetailsCityActivityViewModel {

    public ObservableField<City> city = new ObservableField<>();
    WeatherDetailsCityActivityNavigator navigator;
    Realm realm;
    boolean cityFavored = false;
    long cityId = -1;

    public WeatherDetailsCityActivityViewModel(WeatherDetailsCityActivityNavigator navigator, long cityId) {
        this.navigator = navigator;
        this.cityId = cityId;
        callGetWeatherRequest();
        realm = Realm.getDefaultInstance();
        cityFavored = realm.where(City.class).equalTo("id", cityId).findFirst() != null;
    }

    private void callGetWeatherRequest() {
        navigator.showProgress();
        RetrofitBase.getInterfaceRetrofit()
                .getWeather(cityId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::handleRequestSuccess, this::handleError);
    }

    private void handleError(Throwable throwable) {
        navigator.stopProgress();
        navigator.showMessage(R.string.ops, R.string.connection_error, R.string.try_again, (dialog, which) ->
                callGetWeatherRequest()
        );
    }

    private void handleRequestSuccess(Response<City> response) {
        navigator.stopProgress();
        if (response.code() == 200) {
            city.set(response.body());
            city.notifyChange();
        } else
            navigator.showMessage(R.string.ops, R.string.generic_error, R.string.ok, null);
    }

    public void favorClicked() {
        realm.beginTransaction();
        if (!cityFavored)
            realm.copyToRealm(city.get());
        else
            realm.where(City.class).equalTo("id", city.get().getId()).findFirst().deleteFromRealm();
        cityFavored = !cityFavored;
        navigator.setFavoredStatus(cityFavored);
        realm.commitTransaction();

        if (cityFavored)
            navigator.showShortToast(R.string.add_to_favored_list);
        else
            navigator.showShortToast(R.string.remove_to_favored_list);
    }

    public void updateFavoredState() {
        navigator.setFavoredStatus(cityFavored);
    }

    public boolean isFavored() {
        return cityFavored;
    }
}
