package mateussimoesdossantos.climaapp.scenes.cities.items;

import com.genius.groupie.Item;

import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.databinding.ItemCityBinding;
import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.scenes.cities.items.viewModel.ItemCityViewModel;

/**
 * Created by mateus on 15/08/18.
 */

public class ItemCity extends Item<ItemCityBinding> {

    private ItemCityViewModel mViewModel;

    public ItemCity(City city) {
        mViewModel = new ItemCityViewModel(city);
    }

    @Override
    public int getLayout() {
        return R.layout.item_city;
    }

    @Override
    public void bind(ItemCityBinding viewBinding, int position) {
        viewBinding.setViewModel(mViewModel);
    }
}
