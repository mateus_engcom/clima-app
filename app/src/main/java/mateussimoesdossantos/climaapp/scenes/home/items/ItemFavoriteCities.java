package mateussimoesdossantos.climaapp.scenes.home.items;

import com.genius.groupie.Item;

import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.databinding.ItemFavoriteCitiesBinding;
import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.scenes.home.items.viewModel.ItemFavoriteCitiesViewModel;

/**
 * Created by mateus on 15/08/18.
 */

public class ItemFavoriteCities extends Item<ItemFavoriteCitiesBinding> {

    private ItemFavoriteCitiesViewModel mViewModel;

    public ItemFavoriteCities(City favoriteCity) {
        mViewModel = new ItemFavoriteCitiesViewModel(favoriteCity);
    }

    @Override
    public int getLayout() {
        return R.layout.item_favorite_cities;
    }

    @Override
    public void bind(ItemFavoriteCitiesBinding viewBinding, int position) {
        viewBinding.setViewModel(mViewModel);
    }
}
