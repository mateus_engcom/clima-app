package mateussimoesdossantos.climaapp.scenes.home.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.databinding.ActivityHomeBinding;
import mateussimoesdossantos.climaapp.scenes.base.BaseActivity;
import mateussimoesdossantos.climaapp.scenes.cities.activities.CityChoiceActivity;
import mateussimoesdossantos.climaapp.scenes.home.activities.viewModel.HomeActivityNavigator;
import mateussimoesdossantos.climaapp.scenes.home.activities.viewModel.HomeActivityViewModel;


public class HomeActivity extends BaseActivity implements HomeActivityNavigator {

    ActivityHomeBinding mBinding;
    HomeActivityViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        if (mViewModel == null)
            mViewModel = new HomeActivityViewModel(HomeActivity.this);

        mBinding.setViewModel(mViewModel);
        setToolbar(R.string.weather, false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mViewModel.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_search) {
            //need implementation
            openActivity(CityChoiceActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }
}
