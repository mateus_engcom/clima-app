package mateussimoesdossantos.climaapp.scenes.home.items.viewModel;

import android.databinding.ObservableField;
import android.view.View;

import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.scenes.cities.activities.WeatherDetailsCityActivity;

/**
 * Created by mateus on 15/08/18.
 */

public class ItemFavoriteCitiesViewModel {
    public ObservableField<City> favoriteCity = new ObservableField<>();

    public ItemFavoriteCitiesViewModel(City favoriteCity) {
        this.favoriteCity.set(favoriteCity);
    }

    public void itemClicked(View v) {
        WeatherDetailsCityActivity.openActivity(v.getContext(), favoriteCity.get().getId());
    }
}
