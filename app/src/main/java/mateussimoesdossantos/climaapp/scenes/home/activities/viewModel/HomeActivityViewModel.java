package mateussimoesdossantos.climaapp.scenes.home.activities.viewModel;

import android.databinding.ObservableBoolean;

import com.genius.groupie.GroupAdapter;

import java.util.Collections;

import io.realm.Realm;
import io.realm.RealmResults;
import mateussimoesdossantos.climaapp.R;
import mateussimoesdossantos.climaapp.model.ApiResponses.WeatherResponses.WeatherByCitiesIdsResponse;
import mateussimoesdossantos.climaapp.model.CityModel.City;
import mateussimoesdossantos.climaapp.scenes.home.items.ItemFavoriteCities;
import mateussimoesdossantos.climaapp.service.RetrofitBase;
import retrofit2.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class HomeActivityViewModel {

    public final GroupAdapter favoriteCitiesAdapter = new GroupAdapter();
    public final ObservableBoolean showEmptyPlaceholder = new ObservableBoolean();
    HomeActivityNavigator navigator;
    Realm realm;

    public HomeActivityViewModel(HomeActivityNavigator navigator) {
        this.navigator = navigator;
        realm = Realm.getDefaultInstance();
    }

    private void callGetWeatherRequest() {
        RealmResults<City> list = realm.where(City.class).findAll();
        showEmptyPlaceholder.set(list.size() == 0);
        favoriteCitiesAdapter.clear();

        if (!showEmptyPlaceholder.get()) {
            String citiesIds = "";
            for (City city : list) {
                citiesIds += "," + city.getId();
            }
            navigator.showProgress();
            RetrofitBase.getInterfaceRetrofit()
                    .getListWeather(citiesIds.replaceFirst(",", ""))
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleRequestSuccess, this::handleError);
        }

    }

    private void handleError(Throwable throwable) {
        navigator.stopProgress();
        navigator.showMessage(R.string.ops, R.string.connection_error, R.string.try_again, (dialog, which) ->
                callGetWeatherRequest()
        );
    }

    private void handleRequestSuccess(Response<WeatherByCitiesIdsResponse> response) {
        navigator.stopProgress();
        if (response.code() == 200) {
            Collections.sort(response.body().getList(), (object1, object2) -> (object1.getName()).compareTo((object2.getName())));
            for (City favoriteCity : response.body().getList()) {
                favoriteCitiesAdapter.add(new ItemFavoriteCities(favoriteCity));
            }
            favoriteCitiesAdapter.notifyDataSetChanged();
        } else
            navigator.showMessage(R.string.ops, R.string.generic_error, R.string.ok, null);
    }

    public void onResume() {
        callGetWeatherRequest();
    }
}
