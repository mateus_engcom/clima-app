package mateussimoesdossantos.climaapp.scenes.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.Serializable;

import mateussimoesdossantos.climaapp.R;

/**
 * Created by mateus on 15/08/18.
 */

public class BaseActivity extends AppCompatActivity implements BaseNavigator {

    protected Toolbar toolbar;

    ProgressDialog progress;
    int progressCallCount = 0;
    protected void setToolbar(@StringRes int title) {
        setToolbar(title, true);
    }

    protected void setToolbar(@StringRes int title, boolean backButton) {
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        if (backButton) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }

    public void openActivity(Class cls) {
        startActivity(new Intent(this, cls));
    }

    public void openActivity(Class cls, int requestCode) {
        startActivityForResult(new Intent(this, cls), requestCode);
    }

    public void openActivity(Class cls, int requestCode, String extra, Boolean extraValue) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(extra, extraValue);
        startActivityForResult(intent, requestCode);
    }

    public void openActivity(Class cls, int requestCode, String extra, String extraValue) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(extra, extraValue);
        startActivityForResult(intent, requestCode);
    }

    public void openActivity(Class cls, int requestCode, String extra, Serializable extraValue) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(extra, extraValue);
        startActivityForResult(intent, requestCode);
    }

    public void openActivity(Class cls, String extra, Serializable extraValue) {
        Intent intent = new Intent(this, cls);
        intent.putExtra(extra, extraValue);
        startActivity(intent);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showMessage(@StringRes int title, @StringRes int content, @StringRes int btnMessage, MaterialDialog.SingleButtonCallback callback) {
        new MaterialDialog.Builder(this)
                .title(title)
                .content(content)
                .positiveText(btnMessage)
                .onPositive(callback)
                .show();
    }

    @Override
    public void showMessage(@StringRes int title, @StringRes int content, @StringRes int txtRefuse, @StringRes int txtAccept, MaterialDialog.SingleButtonCallback acceptCallback, MaterialDialog.SingleButtonCallback refuseCallback) {
        new MaterialDialog.Builder(this)
                .title(title)
                .content(content)
                .negativeText(txtRefuse)
                .positiveText(txtAccept)
                .onPositive(acceptCallback)
                .onNegative(refuseCallback)
                .show();
    }

    @Override
    public void showShortToast(int content) {
        Toast.makeText(this, content, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(@StringRes Integer content) {
        if (content == null)
            content = R.string.wait;
        if (progressCallCount == 0) {
            if (progress == null)
                progress = new ProgressDialog(this);
            progress.setMessage(getString(content));
            progress.setProgressStyle(R.style.Theme_AppCompat_Light);
            progress.setCancelable(false);
            progress.show();
        }
        progressCallCount++;
    }

    @Override
    public void showProgress() {
        showProgress(null);
    }

    @Override
    public void stopProgress() {
        if (progressCallCount == 1 && progress != null)
            progress.cancel();

        if (progressCallCount > 0)
            progressCallCount--;
    }
}
