package mateussimoesdossantos.climaapp.scenes.base;

import android.content.Context;
import android.support.annotation.StringRes;

import com.afollestad.materialdialogs.MaterialDialog;

/**
 * Created by mateus on 15/08/18.
 */

public interface BaseNavigator {
    Context getContext();

    void showMessage(@StringRes int title, @StringRes int content, @StringRes int btnMessage, MaterialDialog.SingleButtonCallback callback);

    void showMessage(@StringRes int title, @StringRes int content, @StringRes int txtRefuse, @StringRes int txtAccept, MaterialDialog.SingleButtonCallback acceptCallback, MaterialDialog.SingleButtonCallback refuseCallback);

    void showShortToast(@StringRes int content);

    void stopProgress();

    void showProgress(@StringRes Integer id);

    void showProgress();
}
