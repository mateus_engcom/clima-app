package mateussimoesdossantos.climaapp.utils;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Created by mateus on 15/08/18.
 */

@GlideModule
public class MyAppGlideModule extends AppGlideModule {
}
