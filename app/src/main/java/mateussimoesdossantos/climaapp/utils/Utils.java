package mateussimoesdossantos.climaapp.utils;

import android.content.Context;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;

/**
 * Created by mateus on 15/08/18.
 */

public class Utils {
    static public String loadJSONFromAsset(Context context, String fileName) {
        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    static public void setGlideImageView(ImageView imageView, String url, int drawableError) {
        GlideApp.with(imageView.getContext())
                .load(url)
                .placeholder(drawableError)
                .centerCrop()
                .into(imageView);
    }

    static public String formatDouble2DigitsString(Double s) {
        String rs = "";
        if (s != null) {
            DecimalFormat df = new DecimalFormat();
            df.setMaximumFractionDigits(2);
            df.setMinimumFractionDigits(0);
            rs = df.format(s);
            if (rs.contains(",") && Double.parseDouble(rs.split(",")[1]) == 0)
                rs = rs.substring(0, rs.indexOf(","));
            return rs;
        }

        return rs;
    }
}
