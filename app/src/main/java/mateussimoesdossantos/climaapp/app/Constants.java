package mateussimoesdossantos.climaapp.app;

/**
 * Created by mateus on 15/08/18.
 */

public class Constants {
    static public String fileListJson = "city.list.json";

    public class Service {
        public static final String API_KEY_OPENWEATHERMAP = "2bac87e0cb16557bff7d4ebcbaa89d2f";
        public static final String WEATHER = "weather?appid=" + API_KEY_OPENWEATHERMAP + "&lang=pt&units=metric";
        public static final String GROUP_WEATHER = "group?appid=" + API_KEY_OPENWEATHERMAP + "&lang=pt&units=metric";
        public static final String API_VERSION = "2.5";
        public static final String URL_BASE = "http://api.openweathermap.org/data/" + API_VERSION + "/";
    }
}
