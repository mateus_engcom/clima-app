package mateussimoesdossantos.climaapp.app;

import android.app.Application;

import io.realm.Realm;
import mateussimoesdossantos.climaapp.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by mateus on 15/08/18.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(getString(R.string.nunito_regular))
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
