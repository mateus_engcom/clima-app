package mateussimoesdossantos.climaapp.app;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import mateussimoesdossantos.climaapp.utils.Utils;

/**
 * Created by mateus on 15/08/18.
 */

public class DataBinding {
    @BindingAdapter(value = {"adapter", "layoutManager"})
    public static void setAdapter(RecyclerView recyclerView, RecyclerView.Adapter adapter, RecyclerView.LayoutManager layoutManager) {
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter(value = {"android:visibility"})
    public static void setVisibility(View v, boolean visibility) {
        v.setVisibility(visibility ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter(value = {"src", "errorDrawable"})
    public static void setSrc(ImageView img, String src, int errorDrawable) {
        Utils.setGlideImageView(img, src, errorDrawable);
    }
}
