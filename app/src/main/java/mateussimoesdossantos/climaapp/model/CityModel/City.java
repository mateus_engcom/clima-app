package mateussimoesdossantos.climaapp.model.CityModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by mateus on 15/08/18.
 */

public class City extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("coord")
    @Expose
    private Coord coord;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("main")
    @Expose
    private Main main;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("weather")
    @Expose
    private RealmList<Weather> weather;
    @SerializedName("zoom")
    @Expose
    private int zoom;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Coord getCoord() {
        return coord;
    }

    public void setCoord(Coord coord) {
        this.coord = coord;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(RealmList<Weather> weather) {
        this.weather = weather;
    }

    public String getWeatherDescription() {
        if (weather != null && weather.size() > 0) {
            return weather.get(0).getDescription();
        }
        return null;
    }

    public String getUrlIcon() {
        if (weather != null && weather.size() > 0) {
            return "http://openweathermap.org/img/w/" + weather.get(0).getIcon() + ".png";
        }
        return null;
    }
}
