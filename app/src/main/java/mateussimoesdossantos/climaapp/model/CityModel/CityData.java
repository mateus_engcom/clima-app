package mateussimoesdossantos.climaapp.model.CityModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mateus on 15/08/18.
 */

public class CityData {
    @SerializedName("data")
    @Expose
    List<City> cities;

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }
}
