package mateussimoesdossantos.climaapp.model.ApiResponses.WeatherResponses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import mateussimoesdossantos.climaapp.model.CityModel.City;

/**
 * Created by mateus on 15/08/18.
 */

public class WeatherByCitiesIdsResponse {
    @SerializedName("list")
    @Expose
    private RealmList<City> list;

    public RealmList<City> getList() {
        return list;
    }

    public void setList(RealmList<City> list) {
        this.list = list;
    }
}
