package mateussimoesdossantos.climaapp.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import mateussimoesdossantos.climaapp.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static mateussimoesdossantos.climaapp.app.Constants.Service.URL_BASE;

/**
 * Created by mateus on 15/08/18.
 */

public class RetrofitBase {
    private static RestApi interfaceRetrofit;

    private RetrofitBase() {
        initRetrofit();
    }

    public static RestApi getInterfaceRetrofit() {
        if (interfaceRetrofit == null) new RetrofitBase();
        return interfaceRetrofit;
    }

    void initRetrofit() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            client.addInterceptor(logInterceptor);
        }
        //add timeout
        client.readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);


        final GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithoutExposeAnnotation();
        final Gson gson = builder.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .client(client.build())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        interfaceRetrofit = retrofit.create(RestApi.class);
    }
}
