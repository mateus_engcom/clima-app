package mateussimoesdossantos.climaapp.service;


import mateussimoesdossantos.climaapp.app.Constants;
import mateussimoesdossantos.climaapp.model.ApiResponses.WeatherResponses.WeatherByCitiesIdsResponse;
import mateussimoesdossantos.climaapp.model.CityModel.City;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by mateus on 15/08/18.
 */

public interface RestApi {
    @GET(Constants.Service.WEATHER)
    Observable<Response<City>> getWeather(@Query("id") long id);

    @GET(Constants.Service.GROUP_WEATHER)
    Observable<Response<WeatherByCitiesIdsResponse>> getListWeather(@Query("id") String id);
}
