# Clima APP

Aplicativo de climas para exibir a temperatura de algumas cidades e ainda conta com opção de favoritar cidades.

## Instalação

 `minSdkVersion` deve ser >= 15.

### Arquitetura

  * MVVM Pattern - foi adotada essa arquitetura para aproveitar melhor os recursos do DataBinding no android, para reaproveitar código de maneira mais eficiente e customizar atributos nas views, além de separar a responsabilidade de cada componente na aplicação.

### Bibliotecas usadas
*  [groupie] - Facilita a criação de listas utilizando o recicleview
*  [Retrofit] - Utilizada para consumir api Rest
*  [Material Dialogs] - Facilita a customização de dialogs
*  [glide] - Permite exibir imagens da web de maneira simples e conta com funções como cache de imagens e transformações
*  [Realm] - Utilizada para salvar dados no banco de dados do android com mais facilidade
*  [calligraphy] - Permite adicionar fonts de texto alternativas no android

  

### O que poderia ficar melhor

* O layout da aplicação
* Poderia ter criado alguns testes automatizados


 [groupie]: <https://github.com/Genius/genius-groupie>
 [Retrofit]: <http://square.github.io/retrofit/>
 [Material Dialogs]: <https://github.com/afollestad/material-dialogs>
 [glide]: <https://github.com/bumptech/glide>
 [Realm]: <https://github.com/realm/realm-java/>
 [calligraphy]: <https://github.com/chrisjenx/Calligraphy>